---
title: Over mij
comments: false
---

Mijn naam is **Ricardio**, en onderstaande zijn enkele kwaliteiten van mij:

- Ik wil altijd nieuwe dingen leren
- Ik ben altijd oplossend gericht
- Ik wil anderen helpen

Vanwege dat laatste punt schrijf ik nu mede deze blog. Het is namelijk indirect ook mijn bedoeling om u te informeren over mijn denkwijze en misschien dat u er ook nog wat aan hebt!

