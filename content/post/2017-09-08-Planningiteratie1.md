---
title: Planning iteratie 1
date: 2017-09-08
---

Vandaag de planning en takenverdeling van het team gemaakt (zie afbeelding 1), waarbij ik door Anne van Eijsden werd geassisteerd. Daarnaast heb ik nog wat zitten nadenken over een eventuele naam voor ons spel. Dit deed ik door simpelweg letters te combineren van de woorden die ik bij de doelgroep en het thema vond passen (zie afbeelding 2). Ook heb ik alvast een puzzel gemaakt voor het spel. 
Helaas heb ik hier slechts een erg wazige foto van (zie afbeelding 3), dus opschrijf ik deze even. De was een foto van een verlichte erasmusbrug, die ik op karton had geplakt. Deze heb ik weer in 6 stukken (de hoeveeelheid locaties) gesneden en voila, je hebt een puzzle.

![alt tekst](../img/Planningiteratie1.png)
![alt tekst](../img/Bostro.JPG)
![alt tekst](../img/Puzzle.png)