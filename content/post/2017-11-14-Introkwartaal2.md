---
title: Intro Paard + keuzevakken
date: 2017-11-14
---
##Intro Paard

Vandaag zijn we weer begonnen met een nieuw kwartaal. Hierin is het de bedoeling dat wij voor uitgaansgelegenheid Paard in Den Haag een manier gaan verzinnen om publiek vaker dan één keer te laten komen.

![alt tekst](../img/Paard1.JPG)
![alt tekst](../img/Paard2.JPG)

##Keuzevakken

Ook ben ik begonnen met de keuzevakken Creatief denken en Photoshop ter verbreding van mijn kennis en vooruitgang op deze opleiding