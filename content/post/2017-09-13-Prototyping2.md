---
title: Prototyping 2.0
date: 2017-09-13
---

Vandaag het eerste deel van de workshop prototyping gehad. Hier heb ik met name geleerd hoe makkelijk het kan zijn om snel een prototype te maken, en hoe makkelijk deze prototypes zijn om andere mensen jouw gedachten te laten zien. 
In deze workshop heb ik samen met Nick (1F), Wesley (1F) en Shreyas (dacht 1G) diverse producten gemaakt. Als eerst hoe we het leven makkelijker konden maken.
Daar kwam ons geweldige idee van een alles in 1 handgun uit. Dit apparaat kon je auto wassen, maar was tegelijkertijd ook een portable tankstation zodat je nooit zonder benzine komt te staan. Helaas heb ik hier geen foto van gemaakt.
De tweede opdracht was om een idee te bedenken dat het leven gezonder maakt op school. Dit heb ik samen met Shreyas gedaan en daar is het volgende resultaat uit gekomen.

![alt tekst](../img/Prototypeworkshop.JPG)

Dit prototype stelt een optrek-bar voor waarbij je 5x je hoofd tegen de knop moet drukken door je aan de bar op te trekken. Na dit 5x gedaan te hebben kan de deur pas open.
Dit is een manier waardoor je verplicht wordt om sportief bezig te zijn. Ook als je denkt dat je de knop 5x gewoon met je hand wilt indrukken zul je moeten springen, dus je moet koste wat het kost bewegen.