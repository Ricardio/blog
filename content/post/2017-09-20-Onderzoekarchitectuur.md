---
title: Architectuur
date: 2017-09-20
subtitle: in Rotterdam
---

Vandaag hadden wij een werkcollege wat verder inging op het gebruik van de Double Diamond. Dit is een proces wat relatief gezien weinig tijd kost en met behulp van een COCD-box is het heel makkelijk om bepaalde ideeën te ordenen. Ook heb ik vandaag wederom kunnen zien hoe simpel het kan zijn om snel een prototype te maken en te testen. Dit kan ik dan ook meenemen voor onze Design Challenge.
Tijdens het werkcollege moesten wij een oplossing vinden om studenten te verbinden met een deur. De volgende afbeeldingen geven als eerst de denkwijze weer, met daarna het resultaat.
![alt tekst](../img/Teambrainstormiteratie2.JPG)
![alt tekst](../img/Teamdeurwerkcollege.JPG)
![alt tekst](../img/Teamdeurwerkcollege2.JPG)
![alt tekst](../img/Teamdeurwerkcollege3.JPG)
![alt tekst](../img/Teamdeurwerkcollege4.JPG)
Ook heb ik mijn individuele onderzoek naar de architectuur afgerond en gevisualiseerd. Dit deed ik door van alle architectische bouwwerken die ik onderzocht heb informatie te noteren, en daarnaast een visuele collage te maken. Deze collage geeft een overzichtelijk beeld van verschillende architectische stylen.
![alt tekst](../img/Onderzoekscollage.png)
![alt tekst](../img/Architectuurtekst.png)