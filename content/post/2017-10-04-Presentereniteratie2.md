---
title: Presentatie iteratie 2 + toepassen feedback
date: 2017-10-04
---
## Presentatie

Een daagje vol met presentaties...Wel een beetje gespannen want er zou een alumni komen (een afgestudeerde) Samen met Jari Luijendijk heb ik namens ons team (team SPIRIT) ons spel gepresenteerd. Opzich was de feedback vooral positief! Dit houdt dan ook in dat wij voorlopig denken dat we op de goede weg zitten als het gaat om gedachtengangen over ons spel en de opbouw van ons ontwerpproces.
Uiteraard is het nog wachten op de feedback die wij opgestuurd zullen krijgen, maar voorlopig oogt alles positief. 
Wel kregen we ook feedback van een medestudent, en daar moeten we in iteratie 3 ook echt achteraan. Dit houdt in dat we in iteratie 3 met ons doelgroep de spelanalyse moeten gaan doen, en niet medestudenten in CMD. Opzich is dit goed te bewerkstelligen.
![alt tekst](../img/Presentatie-iteratie-2.png)

## Feedback toepassen

Ook ben ik vandaag na de presentaties nog druk aan de slag gegaan met het verbeteren van mijn moodboard en collage van iteratie 1. Ik heb de feedback toegepast en dat heeft in het volgende geresulteerd:

Moodboard:
![alt tekst](../img/Feedbackmoodboard2.png)
![alt tekst](../img/Feedbackcollage2.png)