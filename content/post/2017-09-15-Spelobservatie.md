---
title: Spelobservatie 
date: 2017-09-15
---

Vandaag gingen wij spelanalyse doen. Hier heb ik vooral het papierwerk gedaan. Mijn aandeel binnen deze observatie/analyse was het opstellen van de speelwijze, de conclusie, en het aanpassen van het onderzoeksrapport (mede mogelijk door Jari Kleine). Wij hebben dan wel de doelgroep ‘Bouwkunde Studenten’, maar wij hadden deze observatie gespeeld met teamgenoten. Dit is dan ook iets waar ik achteraf niet heel tevreden over ben, aangezien dit een vertekend beeld kan scheppen.  Daarnaast hebben wij ook nog gewerkt aan de PowerPoint presentatie die wij dinsdag (19-09-17) zullen hebben. Ook heb ik vandaag het tweede deel van de workshop prototyping gehad, hier heb ik geleerd hoe ik de juiste mensen moet kiezen om tot een goed resultaat/prototype te komen. Nu was ik bij een groepje gaan zitten waar weinig serieus uit kwam waardoor het lastig was om de tijd nuttig te benutten. 

![alt tekst](../img/Uitlegspeelwijzeiteratie1.png)
![alt tekst](../img/Conclusieonderzoeksrapport1.png)