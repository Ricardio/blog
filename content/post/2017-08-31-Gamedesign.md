---
title: Gamedesign
date: 2017-08-31
---

Op deze donderdag hebben wij een hoorcollege gehad over Gamedesign. Tijdens dit hoorcollege kregen wij zeer veel informatie over de doelen en opbouwen van games. De informatie die vooral relevant is voor ons project is dat we de gameplay echt op de nummer 1 moeten gaan zetten, en niet puur op het optische genot uitgaan. Ook zullen we onze game met de volgende bouwstenen moeten gaan bouwen: Doel, Keuzes en Regels. Deze bouwstenen zullen wij nauwkeurig moeten uitwerken en onderzoeken om tot een goed resultaat te kunnen komen. 
