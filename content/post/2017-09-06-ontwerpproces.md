---
title: Ontwerpproces
date: 2017-09-06
---

Vandaag een werkcollege gehad waarin wij veel moesten nadenken over een ontwerpproces wat we eerder in ons leven al onbewust meegemaakt hadden. Dit deden we door het proces, waardoor wij van de bestaande situatie bij de gewenste situatie kwamen, te beschrijven. Dit moesten wij presenteren voor de klas. Door te luisteren naar deze presentaties, en deze terug te koppelen naar mijn eigen ‘ontwerpproblemen’ kwam ik erachter dat eigenlijk het hele proces afhangt van de feedback die jij als ontwerper krijgt van andere mensen. Zonder positieve feedback zal je elke keer opnieuw je ontwerp moeten aanpassen of vernieuwen. Dit is wel iets wat ik kan meenemen in mijn persoonlijke ontwikkeling als CMD-er.
ALs je het kort samenvat, vormt het hele proces een soort kringloop. Deze heb ik onderstaand gevisualiseerd.

![alt tekst](../img/Kringloopwerkcollege.JPG)

Daarnaast ben ik thuis ook alvast een soort van onderzoeksmindmap gaan maken om meer te weten te komen over de doelgroep.

![alt tekst](../img/Persoonlijkebrainstorm.JPG)