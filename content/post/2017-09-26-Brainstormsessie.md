---
title: Brainstormsessie
date: 2017-09-26
---

## Brainstormsessie

Vandaag zijn we begonnen met het brainstormen over het nieuwe concept van ons spel. Dit deden wij door de lessen uit het werkcollege toe te passen. Wij maakten dan ook eerst een long list met heel veel ideeeën, en deze hebben wij samengevat tot een shortlist. De verschillende ideeeën uit deze shortlist gingen wij vervolgens weer ordenen door middel van de COCD-box. Mijn aandeel was om ideeeën te bedenken en daarnaast ook alle ideeeën van de rest van het team noteren op memo-blaadjes, en deze continue ordenen.
Voor mijzelf merkte ik dat deze manier van denken een effectieve manier van ideeeën genereren is. Daarnaast merkte ik ook dat ik tijdens een chaos in een teamverband beter een soort van een leegte in mijn hoofd moet maken, zodat het helemaal rustig en stil is in mijn hoofd. Dit klinkt misschien gek, maar wanneer er vervolgens een bepaald woord vanuit de discussie mijn hoofd binnendringt komt er wel een idee omhoog wat mij de moeite waard lijkt.
Dit idee had ik al wel eerder, alleen dacht ik altijd dat die ideeën nutteloos zouden zijn, maar zoals vandaag had ik dan voor het spel wel een soort van puntensysteem bedacht wat neer kwam op een verre afstammeling van een soort 'Trivia Crack' met verschillende kaarten die je kan ruilen voor materialen die je aan het eind van het spel weer kan gebruiken. Uiteindelijk werdt dit idee wel verder uitgewerkt door de groep en zit dan ook in het voorlopige concept. Dit is dus ook meteen een leer voor mij geweest om mijn ideeeën toch aan de groep voor te stellen, ookal lijken ze nog zo nutteloos.
Totslot hebben Anne en ik ook iteratie 1 en het begin van iteratie 2 in beeld gebracht op de ontwerpproceskaart.

Alle ideeeën op 1 grote hoop:
![alt tekst](../img/Brainstormchaos.JPG)

Het ordenen:
![alt tekst](../img/Brainstormordenen.JPG)
![alt tekst](../img/Brainstormordenen2.JPG)

COCD-box:
![alt tekst](../img/Brainstorm6.JPG)

Vervolg van het brainstormen: (hier werden alle elementen die wij in ons concept wouden hebben uitgewerkt om het passend te maken voor het concept)
![alt tekst](../img/Brainstorm7.JPG)

Ontwerpproceskaart:
![alt tekst](../img/Ontwerpproceskaartiteratie1.JPG)


## Peerfeedback

Ook hadden wij vandaag een opdracht van Studie Coaching. Hierin was het de bedoeling dat drie van je teamgenoten jou zouden beoordelen op verschillende punten.
Hieruit heb ik verschillende punten geleerd:

**Positief:** 

- Ik toon initiatief
- Ik kom afspraken na
- Ik weet in lastige momenten mensen te motiveren
- Ik luister naar mensen en vraag ook door, en vat dit makkelijk samen en deel dit met de rest
- Ik weet mensen te overtuigen (Waaronder opdrachtgever/team/docenten)
- Ik weet in een discussie te bemiddelen
- Ik wil graag leren
- Ik wil graag wat doen voor het team

**Negatief:**

- Ik moet de rest van de groep meer van feedback voorzien (met name via de Facebook)
- Ik moet niet teveel gaan doen binnen het team

**Leerdoelen:**

Voor mezelf zal ik erachteraan willen dat ik de positieve feedback zo wil houden. Daarnaast neem ik ook mijn negatieve feedback mee en zal ik dan ook mijn team van meer feedback gaan voorzien. 

![alt tekst](../img/peerfeedback.JPG)
![alt tekst](../img/peerfeedback2.JPG)
