---
title: Laatste Loodjes
date: 2017-10-24
---
Bijna klaar met deze Design Challenge, morgen is de expo waar alle teams hun spel moeten laten zien. 
Er moest dus nog wel het een en ander gebeuren.
Zo moesten er nog een filmpje gemaakt worden voor de expo die we op een laptop laten afspelen. Het was de bedoeling dat ik dit samen met Anne van Eijsden zou gaan doen, alleen door een gebrek aan ervaring met AfterEffects, heb ik meer toegekeken en indien nodig aangestuurd, met de intentie om er van te leren zodat ik in het vervolg wel zelf ook een filmpje kan maken.
Daarnaast heb ik vandaag verder gewerkt aan de opmaak van de vraagkaarten die ik heb gemaakt. Deze heeft Jari Kleine daarna opgenomen in zijn arsenaal aan vraagkaarten.
Tot slot heb ik samen met Katja Hilgersom de ontwerpproceskaart van iteratie 3 geplakt.

Filmpje
![alt tekst](../img/Filmpjemaken.png)

![alt tekst](../img/Filmpjeaf.png)

Vraagkaarten:
![alt tekst](../img/Vraagkaarten2.jpg)

Ontwerpproceskaart iteratie 3:
![alt tekst](../img/Ontwerpproceskaartiteratie2.JPG)