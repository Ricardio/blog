---
title: Eerste studiodag
date: 2017-09-05
---

Onze eerste dag in de studio. Vandaag hebben we een goede planning gemaakt met het team. Daarnaast zijn er ook goede afspraken gemaakt die gedurende dit project zullen gelden (deze zijn apart ingeleverd op N@tschool). Tijdens deze uren in de studio hebben we ook voorgenomen om allemaal een dezer dagen onze individuele opdrachten thuis af te ronden zodat wij de tijd op school in de studio vol kunnen gebruiken om aan de opdracht te werken en te brainstormen. Tijdens onze brainstorm kwamen wij op de volgende ideeën voor doelgroep en thema: als doelgroep ‘Hogeschool Rotterdam studenten Bouwkunde’, en als thema ‘Constructies in Rotterdam’. Wij zijn dan ook nog Rotterdam ingetrokken om constructies op de foto te zetten voor ons moodboard en collage.

Onderzoek in de stad:
![alt tekst](../img/Markthalonderzoekstad.JPG)
![alt tekst](../img/Markthalonderzoekstad.JPG)

Moodboard van studenten bouwkunde:
![alt tekst](../img/Moodboardstudenten.png)

Collage constructies in Rotterdam:
![alt tekst](../img/Eerstecollage.JPG)
