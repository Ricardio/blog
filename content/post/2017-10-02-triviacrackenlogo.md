---
title: Logo verzinnen
date: 2017-10-02
---

Een heerlijk daagje vrij...
Dacht het niet, gewoon lekker aan school werken dus. 
Vandaag heb ik dan ook weer verschillende taken voor ons concept gedaan.

## Spelanalyse Trivia Crack
Allereerst had ik nog een spelanalyse nodig wat relevant was op ons spel. Aangezien Trivia Crack een vrij bekendt spel is, en ook matcht met ons spel vanwege de vragen, heb ik dus voor Trivia Crack gekozen.
Hier heb ik gewoon een spelanalyse gemaakt, niet veel bijzonders, staat allemaal in de bestanden die ingeleverd zijn.
![alt tekst](../img/SpelanalyseTC.png)

## Naam Concept
Vervolgens ben ik gaan nadenken over een  naam voor ons spel. 
Ik zat als eerst te denken aan verschillende materialen, waardoor de engelse vertaling van baksteen mij wel aansprak, namelijk 'Brick'.
Dus ik ging kijken of ik deze naam nog meer kon onderbouwen, en ja dat was nog niet eens zo gek moeilijk.
- Zo staat de 'B' voor Bouwkunde (opleiding)
- De 'R' voor Rotterdam (locatie)
- De 'I' voor Intro (doel)

Alleen voor de 'ck' wist ik niets te verzinnen en dus ging ik kijken wat ik met deze klank kon. Zo kwam ik erop dat je een 'G' met een bepaalde toon kan uitspreken als 'k' (als in game).
Aangezien het een spel is wat wij moeten maken, was het wordt 'Game' gauw geassocieeërd met de 'G' en werdt de naam dus **BRIG**. 

Het is nog onbekend of deze naam dan ook daadwerkelijk gebruikt zal worden.
![alt tekst](../img/Logo1.JPG)

## Logo Concept
Tot slot moest er nog een logo komen voor ons concept.
Als eerst zat ik een beetje losse schetsen te maken, maar hier was ik nog niet geheel over tevreden, en dus ging ik verschillende kernwoorden opschrijven die mij zouden kunnen inspireren voor mijn logo.
Hierdoor kwam ik op het woordje **'bouwhelm'** wat dan ook samen met de naam een logo zal gaan vormen. In mijn schetsen zijn verschillende fases te zien.
![alt tekst](../img/Logo2.JPG)
![alt tekst](../img/Logo3.JPG)