---
title: Prototyping Iteratie 2
date: 2017-09-29
---

Vandaag zijn we sochtends vooral bezig geweest met het vergelijken van elkaars ideeeën van het spel, hier is dan ook een middenweg uit gekomen. Zo is vanuit mijn kant gekomen dat de spelers aan het einde van het spel de verkregen kaartjes kunnen inruilen om tegen verschillende materialen waarmee men een zo hoog mogelijke toren moet bouwen. 
Ook had ik het idee om de gebouwen op de kaart te combineren met de bijnamen van de gebouwen (zie schets in afbeelding 1). Zo hebben wij dan ook een puntenslijper-markthal getekend, welke Anne van Eijsden verder heeft uitgewerkt in Adobe illustrator (zie afbeelding 2) welke opgenomen zal worden in de plattegrond. Dit kan bijvoorbeeld ook bij de erasmusbrug met de bijnaam 'de zwaan' etcetera. 
Verder hebben Anne, Katja en ik het paperprototype voor deze iteratie gemaakt. (zie afbeelding 3)
![alt tekst](../img/Schetspuntenslijper.png)
![alt tekst](../img/Markthalillustratie.png)
![alt tekst](../img/Paperprototype.png)