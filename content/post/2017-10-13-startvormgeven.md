---
title: Begin van de vormgeving
date: 2017-10-13
---
Het begin van iteratie 3... oftewel, feedback vanuit iteratie 2 meenemen en verwerken, en daarnaast verder uitwerken van het concept.
Voor vandaag had iedereen van ons team een styleguide gemaakt om zo de beste elementen te selecteren om ons concept mee op te bouwen.
Helaas wist ik door het missen van ervaring niet wat een styleguide was, en had ook gegoogled op het begrip 'huisstijl' maar hier werd ik niet veel wijzer van. Hierdoor kwam mijn werk dan ook niet proffessioneel over, en hebben we onze team-styleguide dan ook uit de styleguide's van de andere teamgenoten gehaald die wel wisten hoe zij dit moesten aanpakken.
Na het als team selecteren van een team-styleguide, zijn we begonnen met het visueel vormgeven van ons spel (poging mijn huisstijl: zie afbeelding 1).
Zo heb ik in overleg met het team besloten dat ik het online-gedeelte uit zal werken in Experience Design van Adobe CC. Hier heb ik dan ook alvast een globale opbouw voor gemaakt (zie afbeeldingen 2,3,4,5).
Dit zodat we een duidelijk overzicht hebben met wat voor schermen er ontworpen moeten worden, en hoe het spel gespeelt zal worden.
Daarna ben ik gestart met het maken van de vraagkaarten voor de onderwerpen 'inschatten' en 'rekenen'(zie afbeelding 6).
![alt tekst](../img/Poginghuisstijl.png)
![alt tekst](../img/Appopzet1.png)
![alt tekst](../img/Appopzet2.png)
![alt tekst](../img/Appopset3.png)
![alt tekst](../img/Appopzet4.png)
![alt tekst](../img/vraagkaarten.png)
