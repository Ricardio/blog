---
title: Werkcollege 1 kwartaal 2
date: 2017-11-22
---
Vandaag enkel werkcollege gehad.
Hier hebben wij de theorie toegepast van het afgelopen Hoorcollege.
Deze had ik helaas door me te verslapen gemist, maar het ging over psychologie, en laat dit nou wel iets zijn waar ik altijd al mee bezig ben. 
Dit vond ik dan ook zeer interessant aan biologie #denkwijzen.
We moesten het gedrag van Cartman analyseren en dit verder uitwerken.
Zie afbeeldingen:

![alt tekst](../img/Cartman1.JPG)


![alt tekst](../img/Cartman2.JPG)


![alt tekst](../img/Cartman3.JPG)


![alt tekst](../img/Cartman4.JPG)