---
title: Eerste schooldag
date: 2017-08-30
---

De eerste echte schooldag… Vandaag hebben wij uitleg gehad van hoe en wat op de Hogeschool Rotterdam en wat er van ons wordt verwacht in het eerste leerjaar. Zo kregen we ook meteen de opdracht te horen door middel van een briefing die wij in het eerste kwartaal van dit jaar mogen gaan uitvoeren. Vanaf dit moment hebben wij 6 weken voordat een alumni (afgestudeerde) ons werk van feedback gaat voorzien, en over 8 weken voordat wij deze opdracht moeten inleveren. 
Ook heb ik nagedacht over een mogelijk spel. Dit zou een soort van ‘trivia crack’ kunnen zijn, maar dan aangepast uiteraard. Dit spel speel je voornamelijk op je telefoon en je krijgt een soort van ‘rad van fortuin’ te zien, alleen geeft dit rad een categorie aan waar je een vraag over kan krijgen. Dit kunnen categorieën zijn zoals de geschiedenis van Rotterdam, gebouwen in Rotterdam, maar er kan ook een categorie tussen zitten om elkaar te leren kennen met persoonlijke aspecten. Tot een offline gedeelte ben ik nog niet gekomen.
