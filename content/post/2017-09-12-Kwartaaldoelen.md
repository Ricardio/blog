---
title: Kwartaaldoelen
date: 2017-09-12
---

Deze dag hebben wij tijdens Coaching mijn kwartaal doelen bepaald. Deze zijn hieronder aangegeven:

- Aan het einde van dit kwartaal beheers ik de benodigde Adobe programma’s (Illustrator, Photoshop, Indesign) zodanig dat ik zelfstandig aan opdrachten (waar deze programma’s voor nodig zijn) kan werken zonder hulp van anderen. Dit toon ik aan nadat ik zelf al diverse opdrachten zo heb afgerond.
- Aan het einde van dit kwartaal beheers ik het volledig beheren van mijn blog, en weet ik exact hoe ik deze bij moet houden, zodat ik hier minder tijd aan kwijt hoef te zijn. Dit toon ik aan door mijn tijd te noteren van hoelang het duurde bij het posten van mijn eerste post, en aan het eind kijken of deze drastisch is verminderd.
- Aan het einde van dit kwartaal wil ik een duidelijk beeld voor mijzelf creëren over hoe je alle studiepunten kan behalen gedurende een studiejaar, zodat ik aan het einde van dit kwartaal, maar ook aan het einde van dit leerjaar de volle punten heb. 
- Aan het einde van dit kwartaal ken ik de rest van mijn klasgenoten allemaal zodat ik aan niemand meer hoef te vragen hoe hij/zij heet.

Voor al deze doelen geldt dat ik hier zelf achteraan moet om deze doelen te behalen. Onderweg zal ik ongetwijfeld vragen hebben en deze bij de goede mensen moeten stellen, maar die zoek ik pas op wanneer ik er zelf écht niet uit kom.

Daarnaast heb ik samen met Anne het paperprototype helemaal afgemaakt. Hieronder zijn enkele afbeeldingen hiervan:
![alt tekst](../img/Proto1.png)
![alt tekst](../img/Proto2.png)
![alt tekst](../img/Proto3.png)
![alt tekst](../img/Proto4.png)
![alt tekst](../img/Proto5.png)
