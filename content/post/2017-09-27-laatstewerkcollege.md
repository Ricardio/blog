---
title: Laatste werkcollege van kwartaal 1
date: 2017-09-27
---

Vandaag hadden wij dan het laatste werkcollege van dit kwartaal. Tijdens deze laatste les zijn we desondanks nog aardig productief geweest. 
Dit keer gingen we verder in op het hoorcollege van afgelopen maandag wat ging over onderzoeken. Dit deden wij door te leren om een berg met data te ordenen in verschillende groepjes.

## Lego

Als eerste gingen wij met lego als data deze opdracht doen. Dit deden wij dan ook door de blokjes te ordenen in dimensies zoals grootte, kleur, of vorm. Later moesten wij twee van deze dimensies laten kruisen. Dit deden wij dan ook en dat ging als volgt in zijn werk: 

- Horizontaal: Lego blokjes op kleur van licht naar donker
- Verticaal: Lego blokjes op formaat van groot naar klein

(zie afbeeldingen 1,2,3)

## Foto's

Ook kregen wij een aantal foto's welke wij moesten ordenen. Dit deden wij dan weer door de kruising toe te passen en dus de foto's weer op verschillende dimensies neer te leggen.

- Horizontaal: Foto's van licht naar donker
- Verticaal: Foto's van groot naar klein

(zie afbeeldingen 4,5,6)
Wat ik hier vandaag van heb geleerd is eigenlijk dat als je met verschillende dimensies kan spelen, je eigenlijk best makkelijk je gewonnen data kan ordenen en dit zo makkelijker kan verwerken en interpreteren.

![alt tekst](../img/Lego1.JPG)
![alt tekst](../img/Lego2.JPG)
![alt tekst](../img/Lego3.JPG)
![alt tekst](../img/Lego4.JPG)
![alt tekst](../img/Lego5.JPG)
![alt tekst](../img/Lego9.JPG)

Ook ben ik thuis zelf nog gaan brainstormen over de opbouw van het spel. Hieronder staan enkele afbeeldingen die dit mooi illustreren:
![alt tekst](../img/Thuisbrainstorm.JPG)
![alt tekst](../img/Thuisbrainstorm2.JPG)
![alt tekst](../img/Thuisbrainstorm3.JPG)