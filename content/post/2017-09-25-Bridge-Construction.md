---
title: Bridge Construction
date: 2017-09-25
---

Vandaag hadden wij de 0-meting voor tekenen. Hier was het de bedoeling dat je je eigen hand ging natekenen, wat mij wel goed afging in mijn ogen, en een visual story tekenen, wat mij wat minder afging.
Daarvoor had ik een hoorcollege over onderzoeken. In dit hoorcollege heb ik kennis gemaakt verschillende aspecten van het onderzoeken. Hetgeen wat mij metname duidelijk werdt was dat onderzoek vooral afhangt van reflectie.
Reflectie op je manier van werken, de vergaarde data en op nog tal van dingen. Het is dus zeer belangrijk om tijdens je onderzoek veel te reflecteren op wat je op dat moment heb, of nog moet hebben.
Verder heb ik vandaag ook nog mijn deel van de spelanalyse van 3 bestaande spellen gedaan. Hierbij had ik eerst het online spel 'Happy Wheels' omdat dit een vrij simpel spel is, maar toen ik verder ging denken wat er in de tijd van `Happy Wheels' allemaal nog meer gespeeld werdt, kwam ik uit op het online spel **'Bridge Constructor'**. Dit spel sluit veel meer aan bij de doelgroep aangezien je allemaal constructies moet bouwen. 
![alt tekst](../img/SpelanalyseBC.png)