---
title: Druk daagje
date: 2017-10-03
---

Lekker een drukke dag, er moest nog zeer veel gedaan worden aangezien morgen (4 Oktober) de presentatie en de deadline is en er nog veel puntjes op de 'i' gezet moeten worden.
Als eerst hebben we de naam met logo gekozen. Als naam hebben we mijn naam 'Brig' gekozen in combinatie met het logo van Anne van Eijsden wat een soort van bakstenen voorstelt en dan ook prachtig matched met de naam (zie afbeelding).
Daarnaast ben ik druk bezig geweest met het bouwen van de presentatie. Ook de laatste dia's heb samen met Jari Luijendijk gebouwt. 
Ook moest er nog feedback verwerkt worden dat Mieke de Wilt (studiecoach) ons had gegeven op onze teaminventarisatie en gezamenlijke doelen. Hierbij heeft Katja de gezamenlijke doelen onder handen genomen en ben ik de persoonlijke informatie gaan updaten.
Ook ben ik aan de slag gegaan met de feedback die we van Myrthe Polak (projectcoach) hebben gekregen tijdens de les op ons nieuwe concept, en de feedback die we van haar kregen op ons oude concept.
Ook moest ik nog een CMD beroepsprofiel schrijven en een visueel board maken van een CMD professional in mijn ogen, maar hier ben ik achteraf toch niet helemaal tevreden over.
Met name over de opmaak was ik niet tevreden, het zag er allemaal niet professioneel uit terwijl ik hier wel altijd naar streef. De enige reden waarom ik denk dat het zo is geworden is denk ik omdat ik tot rond 1:00 bezig was met het maken van documenten en het samenvoegen.
Uiteraard hoort dit geen excuus te zijn, maar het was helaas al ingeleverd en dat kon ik niet meer terugdraaien. 
Volgende keer zal ik dus minder hooi op mijn vork moeten nemen, want nu heb ik veel werk ook voor andere gedaan, en dit ging bij mijzelf dus ten nadele van MIJN resultaten, en aangezien ik op school voor mezelf zit, zal ik dit niet meer moeten doen.
![alt tekst](../img/Naam+logo.png)