---
title: Expo + Peerfeedback 2.0 + Kwartaaloverzicht
date: 2017-10-25
---
## Expo

De laatste dag om in teamverband te werken aan dit kwartaal. 
Vandaag was de expo en het was de bedoeling dat hier het eindproduct werd gepresenteerd dmv een pitch. Dit hebben Katja en Anne voor hun rekening genomen.
Mijn taak was mensen naar onze expositie te leiden door bijvoorbeeld mensen aan te spreken en hen op een positieve manier overtuigen om naar onze expositie te gaan kijken.
Daarnaast moest ik verzorgen dat het filmpje getoont werd (waarbij we ineens ook gebruik konden maken van het smartboard) en dat er een telefoon op de telefoon lag met de 'app' die gemaakt was in Adobe Experience Design zodat men deze konden testen en een beetje door de app konden bladeren.
Als ik terug kijk op deze expositie dan is dat wel met een tevreden gevoel. Vandaag zag ik eindelijk uitgebreid hoe ons project vorm heeft gekregen, en met maar liefst 25 stickers was onze score ook wel boven gemiddeld naar mijn idee.
Dit kan je natuurlijk ook weer zien als soort van feedback waardoor je weet dat je eindproduct gewaardeerd wordt door het publiek.
Het gevoel van trots overheerste dan ook bij mij.

![alt tekst](../img/Pitch.JPG)

## Peerfeedback 2.0

Tot slot was er vandaag nog een moment waar we als klas samen kwamen en wij uitleg kregen over het leerdossier. 
Hier moesten we ook weer elkaar beoordelen tijdens een peerfeedback-moment.
Kort samengevat is mijn team net als de vorige peerfeedback weer aardig positief over mij. 
Hieronder zal ik enkele plus- en verbeterpunten op een rijtje zetten:

Positief:
- Ik toon nogsteeds initiatief
- Ik ben aan de slag gegaan met mijn vorige feedback, en ben volgens mijn team sterk verbeterd in het regelen en geven van feedback. Daar ik hier eerst een paar minnetjes kreeg, zijn dit nu louter plusjes geworden.
- In discussies weet ik goed mee te doen en indien nodig een consensus te bereiken
- Ik weet de juiste middelen te gebruiken om mensen te overtuigen en informeer teamleden en docenten over de vooruitgang
- Ik ben bereid om te leren, en ben altijd met iets bezig
- Ik heb altijd mijn spullen op orde

Verbeterpunten:
- Ten opzichte van vorige peerfeedback ben ik op het gebied van de groep motiveren iets omlaag gegaan.
- Het presenteren van mijn eigen ideeeën is iets omlaag gegaan

Als ik zo terug kijk op deze feedback in vergelijking met de feedback die ik tijdens de vorige Peerfeedbacksessie heb gehad, heb ik aardig mijn sterke punten weten door te zetten, en de verbeterpunten aangepast.
Helaas ben ik wel op een paar punten omlaag gegaan. Zo ben ik de laatste iteratie iets minder op de voorgrond gaan staan om mijn teamgenoten te motiveren. Dit omdat ik zowel op school als thuis veel bezig was met de producten voor school waar ik mezelf wat meer op wou focussen.
Dit betekent echter niet dat ik niet mijn team heb geprobeerd te motiveren, maar ik weet ook dat ik hier de laatste iteratie minder mee was. Daarom is deze feedback alleen maar goed en kan ik het meenemen voor in het vervolg.

![alt tekst](../img/Peerfeedback3.JPG)
![alt tekst](../img/Peerfeedback4.JPG)

## Kwartaaloverzicht:

Na vandaag was het kwartaal ook voorbij. Als ik terug kijk op het kwartaal dan verliep het aan het begin wat stroef, maar na een teamevaluatie en duidelijkere afspraken ging het allemaal veel soepeler.
Sterker nog... als ik tijdens de expo keek naar wat wij bereikt hebben met dit team, overheerste het gevoel van trots steeds meer.
Ook omdat ik weet dat ik hier een aardig aandeel in heb gehad. Zo heb ik bijvoorbeeld de volgende beroepsproducten gemaakt:

- Verscheidene moodboards en collages (hier moet ik nog wel aandacht aan besteden om deze beter te krijgen)
- De inrichten van de ontwerpproceskaart van zowel iteratie 1 (samen met Anne en Jari Luijendijk) als iteratie 3 (samen met Katja)
- Het creeeëren van de app in Adobe Experience Design
- Het creeeëren van verscheidene schermen binnen de app
- Verschillende spelanalyses van bestaande spellen
- Een spelanalyse/ observatie van iteratie 1 (samen met Jari Kleine) waarin de beschrijving van de speelwijze, het observatierapport en de conclusie mijn aandeel waren.
- De complete planning van iteratie 1 (inclusief scrumboard)
- De prototypes maken van iteratie 1 (samen met Anne), iteratie samen met Anne en Katja) en iteratie 3 (als team waarvan enkele schermen en de app mijn aandeel waren)
- Het bedenken van een naam en logo voor ons project (Uiteindelijk is mijn naam het geworden in combinatie met het logo van Anne)
- Het maken/bedenken van een prototype (dmv de gevolgde workshop)

Ook heb ik het afgelopen verschillende vaardigheden en kennis vergaard. Dit zijn onder andere:

- Het gebruiken van Adobe Illustrator 
- Het gebruiken van Adobe Experience Design
- Het gebruiken van Adobe After Effects (de basis geleerd van Anne)
- Het inrichten van het ontwerpproces, inclusief het gebruiken van creatieve technieken welke mij zeer helpen met het genereren van ideeeën.
- Het bijhouden van een blog
- Het geven en krijgen van feedback

Aan het eind van het kwartaal ben ik ondanks de vele onduidelijkheden op school toch tevreden met wat ik allemaal heb geleerd.

