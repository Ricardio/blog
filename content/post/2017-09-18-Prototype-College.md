---
title: College Prototyping
date: 2017-09-18
---

Vandaag een hoorcollege gehad wat ging over prototyping. Dit hoorcollege liep natuurlijk achter de feiten aan aangezien wij al onze eerst prototype afhebben. Desondanks geeft dit juist een beter beeld van hoe wij dingen hebben gekozen en gedaan. Hierdoor kan je jezelf een soort van feedback geven, wat weer zorgt dat ons volgende prototype een stuk beter zal moeten worden. Zo zie je maar… de quote “Failure is **success** in **progress**” (*~Albert Einstein~*) gaat ook voor ons op.
