--- 
title: Teamvorming kwartaal 2
date: 2017-11-17
---
Vandaag begonnen we met feedback over hoe het oude team over je dacht. Bij mij kwamen daar de volgende resultaten uit:

**Positief**

- Ik werk altijd door
- Ik wil het beste resultaat
- Ik creeër rust tijdens gesprekken en ben prettig om naast te zitten
- Hou de stress uit de groep
- Toon initatief
- Durf kritiek te hebben

**Verbeterpunten**:

- Ik moet niet teveel werk op me gaan nemen
- Soms iets meer feedback vragen

Ook moesten er vandaag moesten er nieuwe teams gevormd worden. Dit ging op basis van de Belbin test. Voor mij waren de resultaten van deze test dat ik een ras-doener ben. Ik was namelijk Bedrijfsman, en daarnaast ook nog Brononderzoeker wat beide in de categorie van doen valt. Wel grappig, want deze test heb ik aan het begin van kwartaal 1 gedaan, en als ik deze resultaten vergelijk met mijn functie binnen het team van kwartaal 1, dan zie ik wel degelijk dat ik een leider ben en altijd bezig ben met het verduidelijken, presenteren van ideeën tegenover anderen. 
Uiteindelijk ben ik met de volgende mensen in een team gekomen:

-	Meintje Bastemeijer
-   Gabi Broodman
-	Wesley Vink

Een team van 4 dus. Naar mijn mening is dit zelfs beter, omdat je op deze manier misschien wel meer werk zelf moet doen, maar je ook beter met alles betrokken bent.
Al gauw werd ik als de ideale teamcaptain gezien aangezien Gabi (die ook leidinggevende capaciteiten heeft) hier even geen zin in had omdat ze dit vorig kwartaal ook al was.
Echter, ik heb maar liefst 2 keuzevakken op de dinsdag wat inhoud dat ik tot 22:30 onderweg ben, en dus moet oppassen dat ik wel aan mijn eigen taken toe kom, en daarom heb ik ervoor gekozen om deze taak ook niet op me te nemen, maar dit het volgende kwartaal wel op te pakken.
Uiteindelijk wou Wesley graag teamcaptain zijn, en aangezien hij ook de competenties moet halen, besloten we om hem een kans te geven.
Wel heb ik me voorgenomen om indien nodig bij te springen en zijn Wingman te zijn.