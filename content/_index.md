## Welcome

Hallo beste lezer! Welkom op mijn blog.
Ik ben Ricardio en doe de opleiding Communicatie & Multimedia Design aan de Hogeschool Rotterdam. 
Hiervoor moet ik een blog bijhouden om mijn denkwijze en stappen, die ik zet om bij bepaalde eindproducten of doelen te komen, in beeld te brengen.
Op mijn blog zult u dan ook met name lezen hoe ik op bepaalde dingen ben gekomen.

Enjoy!

Ricardio